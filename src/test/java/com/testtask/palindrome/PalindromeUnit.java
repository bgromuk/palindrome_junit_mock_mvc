package com.testtask.palindrome;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Bogdan_Gromiuk on 7/10/2017.
 */

public class PalindromeUnit {

    PalindromeService palindromeService;

    @Before
    public void init() {
        palindromeService = new PalindromeService();
    }


    @Test
    public void testTruePol() {
        String str = "akka";

        assertTrue(palindromeService.isPal(str));
    }

    @Test
    public void testFalsePol() {
        String str = "kaka";

        assertFalse(palindromeService.isPal(str));
    }
}
