package com.testtask.palindrome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Bogdan_Gromiuk on 7/10/2017.
 */
@RestController
public class PalindromeController {

    @Autowired
    PalindromeService palindromeService;

    @RequestMapping(value="/is_palindrome", method = RequestMethod.GET)
    public Boolean isPalindrome(String str) {
        return palindromeService.isPal(str);

    }
}
